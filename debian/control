Source: fastqtl
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Dylan Aïssi <daissi@debian.org>,
           Étienne Mollier <emollier@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3,
               libboost-dev,
               libboost-iostreams-dev,
               libboost-program-options-dev,
               libgsl-dev,
               r-mathlib,
               zlib1g-dev,
               libblas-dev,
               libeigen3-dev,
               libhts-dev,
               libbz2-dev
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/med-team/fastqtl
Vcs-Git: https://salsa.debian.org/med-team/fastqtl.git
Homepage: http://fastqtl.sourceforge.net/
Rules-Requires-Root: no

Package: fastqtl
Architecture: any
Depends: ${shlibs:Depends},
         ${python3:Depends},
         python3,
         ${misc:Depends}
Recommends: r-cran-qvalue,
            r-cran-tools,
            r-cran-argparser
Suggests: fastqtl-doc
Description: Quantitative Trait Loci (QTL) mapper in cis for molecular phenotypes
 The goal of FastQTL is to identify single-nucleotide polymorphisms (SNPs)
 which are significantly associated with various molecular phenotypes
 (i.e. expression of known genes, cytosine methylation levels, etc).
 It performs scans for all possible phenotype-variant pairs in cis
 (i.e. variants located within a specific window around a phenotype).
 FastQTL implements a new permutation scheme (Beta approximation) to accurately
 and rapidly correct for multiple-testing at both the genotype and phenotype
 levels.

Package: fastqtl-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends}
Enhances: fastqtl
Description: QTL mapper in cis for molecular phenotypes - documentation
 The goal of FastQTL is to identify single-nucleotide polymorphisms (SNPs)
 which are significantly associated with various molecular phenotypes
 (i.e. expression of known genes, cytosine methylation levels, etc).
 It performs scans for all possible phenotype-variant pairs in cis
 (i.e. variants located within a specific window around a phenotype).
 FastQTL implements a new permutation scheme (Beta approximation) to accurately
 and rapidly correct for multiple-testing at both the genotype and phenotype
 levels.
 .
 This package provides documentation and example data to work with FastQTL.
